import org.junit.Test;
import tp.backend.ULD;
import tp.backend.db.DatabaseHandler;
import tp.backend.db.SQLHelper;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Stefan on 04.04.2017.
 */
public class ULDTest {
    private DatabaseHandler handler;
    public ULDTest() throws SQLException, ClassNotFoundException {
        handler=new DatabaseHandler("mysql","localhost",3306,
                "root","","tp");
    }
    @Test
    public void testLoadAllULD(){
        SQLHelper helper=new SQLHelper(handler);
        List<ULD> ulds=helper.loadAllULD();
        System.out.println("Size: "+ulds.size());
        System.out.println(ulds);
        System.out.println("Optimal Fit:");
        System.out.println(ulds.get(0).getOptimalFit());
        System.out.println("Non Optimal Fit:");
        System.out.println(ulds.get(0).getNonOptimalFit());
    }
}
