import org.junit.Test;
import tp.backend.Plane;
import tp.backend.ULD;
import tp.backend.db.DatabaseHandler;
import tp.backend.db.SQLHelper;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Stefan on 04.04.2017.
 */
public class PlanesTest {
    private DatabaseHandler handler;
    public PlanesTest() throws SQLException, ClassNotFoundException {
        handler=new DatabaseHandler("mysql","localhost",3306,
                "root","","tp");
    }
    @Test
    public void testLoadPlanes(){
        SQLHelper helper=new SQLHelper(handler);
        List<Plane> planes=helper.loadAllPlanes();
        System.out.println("Size: "+planes.size());
        System.out.println(planes);
        System.out.println("Optimal Fit:");
        System.out.println(planes.get(0).getOptimalFit());
        System.out.println("Non Optimal Fit:");
        System.out.println(planes.get(0).getNonOptimalFit());
    }
    @Test
    public void testLoadOptimalFitBAE(){
        SQLHelper helper=new SQLHelper(handler);
        List<ULD> ulds=helper.loadAllULD();
        for (ULD uld:ulds){
            for (Plane plane:uld.getOptimalFit()){
                if (plane.getName().contains("BAE")){
                    System.out.println(uld+": OF :"+plane);
                }
            }
            for (Plane plane:uld.getNonOptimalFit()){
                if (plane.getName().contains("BAE")){
                    System.out.println(uld+": NOF :"+plane);
                }
            }
        }
    }
}
