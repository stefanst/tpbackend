-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 04. Apr 2017 um 07:30
-- Server Version: 5.5.49-0ubuntu0.14.04.1
-- PHP-Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `tp`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `compatibility`
--

CREATE TABLE IF NOT EXISTS `compatibility` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_plane_id` int(11) NOT NULL,
  `comp_uld_id` int(11) NOT NULL,
  `comp_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=343 ;

--
-- Daten für Tabelle `compatibility`
--

INSERT INTO `compatibility` (`comp_id`, `comp_plane_id`, `comp_uld_id`, `comp_type`) VALUES
(1, 15, 2, 0),
(2, 11, 2, 1),
(3, 29, 2, 1),
(4, 21, 2, 1),
(5, 7, 2, 1),
(6, 25, 2, 1),
(7, 26, 2, 1),
(8, 37, 2, 1),
(9, 15, 3, 0),
(10, 11, 3, 1),
(11, 29, 3, 1),
(12, 21, 3, 1),
(13, 7, 3, 1),
(14, 25, 3, 1),
(15, 26, 3, 1),
(16, 37, 3, 1),
(17, 15, 4, 0),
(18, 2, 5, 0),
(19, 11, 5, 0),
(20, 20, 5, 0),
(21, 29, 5, 0),
(22, 12, 5, 0),
(23, 21, 5, 0),
(24, 30, 5, 0),
(25, 4, 5, 0),
(26, 13, 5, 0),
(27, 16, 5, 0),
(28, 25, 5, 0),
(29, 35, 5, 0),
(30, 26, 5, 0),
(31, 9, 5, 0),
(32, 18, 5, 0),
(33, 27, 5, 0),
(34, 37, 5, 0),
(35, 6, 5, 1),
(36, 15, 5, 1),
(37, 11, 6, 1),
(38, 29, 6, 1),
(39, 21, 6, 1),
(40, 15, 6, 1),
(41, 7, 6, 1),
(42, 25, 6, 1),
(43, 26, 6, 1),
(44, 37, 6, 1),
(45, 23, 7, 0),
(46, 11, 7, 1),
(47, 29, 7, 1),
(48, 21, 7, 1),
(49, 32, 7, 1),
(50, 6, 7, 1),
(51, 15, 7, 1),
(52, 24, 7, 1),
(53, 7, 7, 1),
(54, 16, 7, 1),
(55, 25, 7, 1),
(56, 35, 7, 1),
(57, 26, 7, 1),
(58, 9, 7, 1),
(59, 18, 7, 1),
(60, 27, 7, 1),
(61, 37, 7, 1),
(62, 11, 8, 1),
(63, 29, 8, 1),
(64, 21, 8, 1),
(65, 15, 8, 1),
(66, 7, 8, 1),
(67, 25, 8, 1),
(68, 26, 8, 1),
(69, 18, 8, 1),
(70, 37, 8, 1),
(71, 2, 9, 1),
(72, 11, 9, 1),
(73, 20, 9, 1),
(74, 29, 9, 1),
(75, 12, 9, 1),
(76, 21, 9, 1),
(77, 30, 9, 1),
(78, 4, 9, 1),
(79, 13, 9, 1),
(80, 23, 9, 1),
(81, 32, 9, 1),
(82, 6, 9, 1),
(83, 15, 9, 1),
(84, 34, 9, 1),
(85, 7, 9, 1),
(86, 16, 9, 1),
(87, 25, 9, 1),
(88, 35, 9, 1),
(89, 26, 9, 1),
(90, 36, 9, 1),
(91, 9, 9, 1),
(92, 18, 9, 1),
(93, 27, 9, 1),
(94, 37, 9, 1),
(95, 6, 10, 0),
(96, 15, 10, 0),
(97, 11, 11, 0),
(98, 7, 11, 0),
(99, 15, 11, 1),
(100, 25, 11, 1),
(101, 18, 11, 1),
(102, 37, 11, 1),
(103, 23, 12, 0),
(104, 32, 12, 0),
(105, 11, 12, 1),
(106, 29, 12, 1),
(107, 21, 12, 1),
(108, 15, 12, 1),
(109, 24, 12, 1),
(110, 7, 12, 1),
(111, 25, 12, 1),
(112, 26, 12, 1),
(113, 18, 12, 1),
(114, 37, 12, 1),
(115, 23, 13, 0),
(116, 32, 13, 0),
(117, 11, 13, 1),
(118, 29, 13, 1),
(119, 21, 13, 1),
(120, 15, 13, 1),
(121, 7, 13, 1),
(122, 25, 13, 1),
(123, 26, 13, 1),
(124, 18, 13, 1),
(125, 37, 13, 1),
(126, 11, 14, 1),
(127, 29, 14, 1),
(128, 21, 14, 1),
(129, 5, 14, 1),
(130, 6, 14, 1),
(131, 25, 14, 1),
(132, 26, 14, 1),
(133, 18, 14, 1),
(134, 37, 14, 1),
(135, 11, 15, 1),
(136, 29, 15, 1),
(137, 21, 15, 1),
(138, 5, 15, 1),
(139, 6, 15, 1),
(140, 25, 15, 1),
(141, 26, 15, 1),
(142, 18, 15, 1),
(143, 37, 15, 1),
(144, 10, 16, 0),
(145, 11, 16, 1),
(146, 29, 16, 1),
(147, 21, 16, 1),
(148, 15, 16, 1),
(149, 25, 16, 1),
(150, 26, 16, 1),
(151, 18, 16, 1),
(152, 37, 16, 1),
(153, 19, 16, 1),
(154, 5, 17, 0),
(155, 31, 17, 0),
(156, 11, 17, 1),
(157, 29, 17, 1),
(158, 21, 17, 1),
(159, 15, 17, 1),
(160, 25, 17, 1),
(161, 26, 17, 1),
(162, 18, 17, 1),
(163, 37, 17, 1),
(164, 5, 18, 0),
(165, 31, 18, 0),
(166, 11, 18, 1),
(167, 29, 18, 1),
(168, 21, 18, 1),
(169, 23, 18, 1),
(170, 32, 18, 1),
(171, 15, 18, 1),
(172, 25, 18, 1),
(173, 26, 18, 1),
(174, 18, 18, 1),
(175, 37, 18, 1),
(176, 23, 19, 0),
(177, 32, 19, 0),
(178, 11, 19, 1),
(179, 29, 19, 1),
(180, 21, 19, 1),
(181, 15, 19, 1),
(182, 7, 19, 1),
(183, 25, 19, 1),
(184, 26, 19, 1),
(185, 18, 19, 1),
(186, 37, 19, 1),
(187, 11, 20, 1),
(188, 29, 20, 1),
(189, 21, 20, 1),
(190, 5, 20, 1),
(191, 6, 20, 1),
(192, 25, 20, 1),
(193, 26, 20, 1),
(194, 18, 20, 1),
(195, 37, 20, 1),
(196, 11, 21, 1),
(197, 29, 21, 1),
(198, 21, 21, 1),
(199, 5, 21, 1),
(200, 6, 21, 1),
(201, 25, 21, 1),
(202, 26, 21, 1),
(203, 18, 21, 1),
(204, 37, 21, 1),
(205, 10, 22, 0),
(206, 11, 22, 1),
(207, 29, 22, 1),
(208, 21, 22, 1),
(209, 15, 22, 1),
(210, 25, 22, 1),
(211, 26, 22, 1),
(212, 18, 22, 1),
(213, 37, 22, 1),
(214, 19, 22, 1),
(215, 5, 23, 0),
(216, 31, 23, 0),
(217, 11, 23, 1),
(218, 29, 23, 1),
(219, 21, 23, 1),
(220, 15, 23, 1),
(221, 25, 23, 1),
(222, 26, 23, 1),
(223, 18, 23, 1),
(224, 37, 23, 1),
(225, 5, 24, 0),
(226, 31, 24, 0),
(227, 11, 24, 1),
(228, 29, 24, 1),
(229, 21, 24, 1),
(230, 23, 24, 1),
(231, 32, 24, 1),
(232, 15, 24, 1),
(233, 25, 24, 1),
(234, 26, 24, 1),
(235, 18, 24, 1),
(236, 37, 24, 1),
(237, 5, 25, 1),
(238, 31, 25, 1),
(239, 32, 25, 1),
(240, 15, 26, 0),
(241, 25, 26, 1),
(242, 6, 27, 0),
(243, 15, 27, 0),
(244, 34, 27, 1),
(245, 7, 27, 1),
(246, 16, 27, 1),
(247, 25, 27, 1),
(248, 27, 27, 1),
(249, 37, 27, 1),
(250, 2, 28, 0),
(251, 11, 28, 0),
(252, 20, 28, 0),
(253, 29, 28, 0),
(254, 12, 28, 0),
(255, 21, 28, 0),
(256, 30, 28, 0),
(257, 4, 28, 0),
(258, 13, 28, 0),
(259, 16, 28, 0),
(260, 25, 28, 0),
(261, 35, 28, 0),
(262, 17, 28, 0),
(263, 26, 28, 0),
(264, 36, 28, 0),
(265, 9, 28, 0),
(266, 18, 28, 0),
(267, 27, 28, 0),
(268, 37, 28, 0),
(269, 22, 28, 1),
(270, 32, 28, 1),
(271, 6, 28, 1),
(272, 15, 28, 1),
(273, 34, 28, 1),
(274, 7, 28, 1),
(275, 3, 29, 0),
(276, 27, 29, 1),
(277, 2, 30, 0),
(278, 11, 30, 0),
(279, 20, 30, 0),
(280, 29, 30, 0),
(281, 12, 30, 0),
(282, 21, 30, 0),
(283, 30, 30, 0),
(284, 4, 30, 0),
(285, 13, 30, 0),
(286, 16, 30, 0),
(287, 25, 30, 0),
(288, 35, 30, 0),
(289, 17, 30, 0),
(290, 26, 30, 0),
(291, 36, 30, 0),
(292, 9, 30, 0),
(293, 18, 30, 0),
(294, 27, 30, 0),
(295, 37, 30, 0),
(296, 32, 30, 1),
(297, 6, 30, 1),
(298, 15, 30, 1),
(299, 34, 30, 1),
(300, 7, 30, 1),
(301, 2, 31, 1),
(302, 11, 31, 1),
(303, 20, 31, 1),
(304, 29, 31, 1),
(305, 12, 31, 1),
(306, 21, 31, 1),
(307, 30, 31, 1),
(308, 4, 31, 1),
(309, 13, 31, 1),
(310, 32, 31, 1),
(311, 6, 31, 1),
(312, 15, 31, 1),
(313, 34, 31, 1),
(314, 7, 31, 1),
(315, 16, 31, 1),
(316, 25, 31, 1),
(317, 35, 31, 1),
(318, 17, 31, 1),
(319, 26, 31, 1),
(320, 36, 31, 1),
(321, 9, 31, 1),
(322, 18, 31, 1),
(323, 27, 31, 1),
(324, 37, 31, 1),
(325, 2, 32, 0),
(326, 11, 32, 0),
(327, 20, 32, 0),
(328, 29, 32, 0),
(329, 12, 32, 0),
(330, 21, 32, 0),
(331, 30, 32, 0),
(332, 4, 32, 0),
(333, 13, 32, 0),
(334, 16, 32, 0),
(335, 25, 32, 0),
(336, 35, 32, 0),
(337, 26, 32, 0),
(338, 36, 32, 0),
(339, 9, 32, 0),
(340, 18, 32, 0),
(341, 27, 32, 0),
(342, 37, 32, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `planes`
--

CREATE TABLE IF NOT EXISTS `planes` (
  `plane_id` int(11) NOT NULL AUTO_INCREMENT,
  `plane_name` varchar(128) NOT NULL,
  PRIMARY KEY (`plane_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Daten für Tabelle `planes`
--

INSERT INTO `planes` (`plane_id`, `plane_name`) VALUES
(2, 'Airbus A300'),
(3, 'Airbus A319/320/321'),
(4, 'Airbus A350'),
(5, 'BAE 146-200-QC/QT'),
(6, 'Boeing 747-200/300/400'),
(7, 'Boeing 767F'),
(8, 'Fokker 27'),
(9, 'McDonnell Douglas DC-10'),
(10, 'Military: C-130(Hercules)'),
(11, 'Airbus A300F/300-600F'),
(12, 'Airbus A330-200/300'),
(13, 'Airbus A380'),
(14, 'BAE ATP'),
(15, 'Boeing 747F'),
(16, 'Boeing 777-200/300/400'),
(17, 'Ilyushin IL-86'),
(18, 'McDonnell Douglas DC-10F'),
(19, 'Military: C-17 Globmaster'),
(20, 'Airbus A310'),
(21, 'Airbus A330-200F'),
(22, 'ATR 42/72'),
(23, 'Boeing 727-100C/QC/F'),
(24, 'Boeing 757F'),
(25, 'Boeing 777F'),
(26, 'Ilyushin IL-96'),
(27, 'McDonnell Douglas MD-11'),
(28, 'Military: C-27J Spartan'),
(29, 'Airbus A310 P2F'),
(30, 'Airbus A340-200/300/500/600'),
(31, 'BAE 146-201F'),
(32, 'Boeing 737C/QC/F'),
(34, 'Boeing 767-200/300/400'),
(35, 'Boeing 787'),
(36, 'Lockheed L-1011-200'),
(37, 'McDonnell Douglas MD-11F');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `uld`
--

CREATE TABLE IF NOT EXISTS `uld` (
  `uld_id` int(11) NOT NULL AUTO_INCREMENT,
  `uld_name` varchar(15) NOT NULL,
  `uld_length` int(11) NOT NULL,
  `uld_width` int(11) NOT NULL,
  `uld_height` int(11) NOT NULL,
  `uld_category` int(11) NOT NULL,
  `uld_weight` int(11) NOT NULL,
  `uld_vol_ext` float NOT NULL,
  `uld_vol_int` float NOT NULL,
  PRIMARY KEY (`uld_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Daten für Tabelle `uld`
--

INSERT INTO `uld` (`uld_id`, `uld_name`, `uld_length`, `uld_width`, `uld_height`, `uld_category`, `uld_weight`, `uld_vol_ext`, `uld_vol_int`) VALUES
(2, 'AAA', 3175, 2235, 2438, 0, 6800, 17.3, 15.7),
(3, 'AAB', 3175, 2235, 2438, 0, 0, 0, 0),
(4, 'AAD', 2235, 3175, 2997, 0, 6800, 21.1, 18.18),
(5, 'AAF', 4046, 2235, 1626, 0, 4625, 14.2, 12.7),
(6, 'AAJ', 2235, 3175, 2438, 0, 6800, 16.8, 15.1),
(7, 'AAK', 3175, 2235, 1626, 0, 6033, 10.1, 9.8),
(8, 'AAM', 3175, 2235, 2286, 0, 6800, 15.1, 13.7),
(9, 'AAP', 3175, 2235, 1626, 0, 4625, 11.5, 9.8),
(10, 'AAU', 4724, 2235, 1626, 0, 4625, 15.6, 13.9),
(11, 'AAX', 3175, 2235, 2362, 0, 6033, 14.7, 13.7),
(12, 'AAY', 3175, 2235, 2083, 0, 6033, 15.6, 13.9),
(13, 'AAZ', 3175, 2235, 2083, 0, 6033, 15.6, 13.9),
(14, 'ABA', 2743, 2235, 2438, 0, 4536, 16.3, 0),
(15, 'ABB', 2743, 2235, 2438, 0, 4536, 16.3, 0),
(16, 'ABJ', 2743, 2235, 2438, 0, 4536, 15.8, 0),
(17, 'ABN', 2743, 2235, 1778, 0, 4536, 9.3, 8.6),
(18, 'ABY', 2743, 2235, 2083, 0, 4536, 11.3, 10),
(19, 'AAZ', 3175, 2235, 2083, 0, 6033, 15.6, 13.9),
(20, 'ABA', 2743, 2235, 2438, 0, 4536, 16.3, 0),
(21, 'ABB', 2743, 2235, 2438, 0, 4536, 16.3, 0),
(22, 'ABJ', 2743, 2235, 2438, 0, 4536, 15.8, 0),
(23, 'ABN', 2743, 2235, 1778, 0, 4536, 9.6, 8.6),
(24, 'ABY', 2743, 2235, 2083, 0, 4536, 11.3, 10),
(25, 'AEP', 2235, 1346, 1626, 0, 1818, 4.9, 4.3),
(26, 'AGA', 2438, 6058, 2438, 0, 11340, 36.1, 33),
(27, 'AKC', 2337, 1534, 1626, 0, 1588, 5.2, 4.7),
(28, 'AKE', 2007, 1534, 1626, 0, 1588, 4.8, 4.3),
(29, 'AKH', 2438, 1534, 1143, 0, 1134, 3.9, 3.6),
(30, 'AKN', 2007, 1534, 1626, 0, 1588, 4.8, 4.1),
(31, 'AKP', 1562, 1534, 1626, 0, 1588, 3.9, 3.6),
(32, 'ALF', 4064, 1534, 1626, 0, 3175, 9.6, 8.9);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `uld_categories`
--

CREATE TABLE IF NOT EXISTS `uld_categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `uld_categories`
--

INSERT INTO `uld_categories` (`category_id`, `category_name`) VALUES
(0, 'Aircraft containers'),
(1, 'Aircraft pallets'),
(2, 'Car transporters'),
(3, 'Cool containers'),
(4, 'Horse stables');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
