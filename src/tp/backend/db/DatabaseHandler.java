package tp.backend.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Stefan on 11.01.2017.
 */
public class DatabaseHandler {
    private String type;
    private String host;
    private int port;
    private String user;
    private String password;
    private String database;

    public DatabaseHandler(String type, String host, int port,
                           String user, String password, String database)
            throws ClassNotFoundException, SQLException {
        this.type = type;
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.database=database;
    }
    private String getURL(){
        StringBuilder builder=new StringBuilder();
        builder.append("jdbc:").append(type).append("://").append(host).append(":").append(port).
                append("/").append(database).append("?").
                append("user=").append(user).append("&password=").append(password);
        return builder.toString();
    }

    public Connection getConnection() throws SQLException {
        synchronized (this){
            return DriverManager.getConnection(getURL());
        }
    }

    public void closeConnection(Connection connection){
        try {
            connection.close();
        }catch (Exception e){}
    }

    public void closeStatement(Statement statement){
        try {
            statement.close();
        }catch (Exception e){}
    }
}
