package tp.backend.db;

import tp.backend.Plane;
import tp.backend.ULD;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Stefan on 04.04.2017.
 */
public class SQLHelper {
    private DatabaseHandler handler;

    public SQLHelper(DatabaseHandler handler) {
        this.handler = handler;
    }

    public List<Plane> loadAllPlanes(){
        ArrayList<Plane> ret=null;
        try {
            Connection connection=handler.getConnection();
            Statement statement=connection.createStatement();
            HashMap<Integer,Plane> planes=new HashMap<>();
            ResultSet set=statement.executeQuery("SELECT * FROM planes ORDER BY plane_id");
            while (set.next()){
                planes.put(set.getInt("plane_id"),getPlaneFromResult(set));
            }
            set=statement.executeQuery("SELECT planes.*, compatibility.*, uld.*, uld_categories.* FROM planes" +
                    "  JOIN compatibility ON planes.plane_id=compatibility.comp_plane_id" +
                    "  JOIN uld ON comp_uld_id=uld_id" +
                    "  JOIN uld_categories ON uld.uld_category=uld_categories.category_id " +
                    "ORDER BY plane_id");
            while (set.next()){
                int id=set.getInt("plane_id");
                if (planes.get(id)==null){
                    Plane plane=getPlaneFromResult(set);
                    planes.put(id,plane);
                }
                Plane plane=planes.get(id);
                if (set.getInt("comp_type")==0){
                    plane.getOptimalFit().add(getULDfromResult(set));
                }else {
                    plane.getNonOptimalFit().add(getULDfromResult(set));
                }
            }
            ret=new ArrayList<>(planes.values());
            handler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public List<ULD> loadAllULD(){
        ArrayList<ULD> ret=null;
        try {
            Connection connection=handler.getConnection();
            Statement statement=connection.createStatement();
            ResultSet set=statement.executeQuery("SELECT uld.*, compatibility.*, planes.*, uld_categories.* FROM uld" +
                    "  JOIN compatibility ON uld.uld_id=compatibility.comp_uld_id" +
                    "  JOIN planes ON comp_plane_id=plane_id" +
                    "  JOIN uld_categories ON uld.uld_category=uld_categories.category_id " +
                    "ORDER BY uld_id");
            HashMap<Integer,ULD> ulds=new HashMap<>();
            while (set.next()){
                int id=set.getInt("uld_id");
                if (!ulds.containsKey(id)){
                    ULD uld=getULDfromResult(set);
                    ulds.put(id,uld);
                }
                ULD uld=ulds.get(id);
                if (set.getInt("comp_type")==0){
                    uld.getOptimalFit().add(getPlaneFromResult(set));
                }else {
                    uld.getNonOptimalFit().add(getPlaneFromResult(set));
                }
            }
            ret=new ArrayList<>(ulds.values());
            handler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public Plane getPlaneFromResult(ResultSet set) throws SQLException {
        Plane ret=null;
        if (set!=null){
            ret=new Plane(set.getString("plane_name"));
        }
        return ret;
    }
    public ULD getULDfromResult(ResultSet set) throws SQLException {
        ULD ret=null;
        if (set!=null){
            ret=new ULD(set.getString("uld_name"),
                    set.getInt("uld_length"),
                    set.getInt("uld_width"),
                    set.getInt("uld_height"),
                    set.getString("category_name"),
                    set.getInt("uld_weight"),
                    set.getFloat("uld_vol_ext"),
                    set.getFloat("uld_vol_int"));
        }
        return ret;
    }
}
