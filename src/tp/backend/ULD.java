package tp.backend;

/**
 * Created by Stefan on 04.04.2017.
 */
public class ULD extends Component<Plane> {
    private int length;
    private int width;
    private int height;
    private String category;
    private int weight;
    private float volumeExt;
    private float volumeInt;
    public ULD(String name) {
        super(name);
    }

    public ULD(String name, int length, int width, int height, String category, int weight, float volumeExt, float volumeInt) {
        super(name);
        this.length = length;
        this.width = width;
        this.height = height;
        this.category = category;
        this.weight = weight;
        this.volumeExt = volumeExt;
        this.volumeInt = volumeInt;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public float getVolumeExt() {
        return volumeExt;
    }

    public void setVolumeExt(float volumeExt) {
        this.volumeExt = volumeExt;
    }

    public float getVolumeInt() {
        return volumeInt;
    }

    public void setVolumeInt(float volumeInt) {
        this.volumeInt = volumeInt;
    }
}
