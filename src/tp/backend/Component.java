package tp.backend;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 04.04.2017.
 */
public class Component<T> {
    private File image;
    private String name;
    private List<T> optimalFit=new ArrayList<T>();
    private List<T> nonOptimalFit=new ArrayList<T>();

    public Component(String name) {
        this.name=name;
    }


    public void setImagePath(String path){
        File file=new File(path);
        if (!file.exists()||file.isDirectory()){
            throw new IllegalArgumentException("File doesn't exist or is directory");
        }
        image=file;
    }

    public BufferedImage getBufferedImage() throws IOException {
        BufferedImage ret=null;
        if (image!=null){
            ret= ImageIO.read(image);
        }
        return ret;
    }

    public String getName() {
        return name;
    }

    public List<T> getOptimalFit() {
        return optimalFit;
    }

    public List<T> getNonOptimalFit() {
        return nonOptimalFit;
    }

    @Override
    public String toString() {
        return name;
    }
}
